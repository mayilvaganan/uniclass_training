from optparse import Values
import uuid
import pandas


def generate_uuid(
        import_csv_files: dict,
        updated_csv_files: dict) \
        -> dict:

    for keys, Values \
            in import_csv_files.items():
        Values['unique_id'] = \
            Values.apply(lambda x: uuid.uuid4(), axis=1)
        updated_csv_files.update({keys: Values})
        
    return \
        updated_csv_files
        
