import pandas
import os
import sys

sys.path.append('C:\\Users\\Mayil.VagananM\\PythonDev\\code\\uniclass_training\\')

from uniclass_training_source.b_code.common_function.import_csv \
    import import_csv

from uniclass_training_source.b_code.load_funtion.generate_uuid \
    import generate_uuid

from uniclass_training_source.b_code.common_function.export_csv \
    import export_csv


def orchestrator_load4(
        source_path: str,
        output_path: str) \
        -> None:
    input_files = {}
    updated_files = {}

    list_files = \
        os.listdir(source_path)

    import_csv_files = \
        import_csv(list_files, source_path, input_files)

    generating_uuid = \
        generate_uuid(import_csv_files, updated_files)

    export_csv(generating_uuid, output_path)
