import pandas


def export_csv(
        import_csv_files: dict,
        output_path: str
) \
        -> None:
    for keys, values \
            in import_csv_files.items():
        print(keys)
        values.to_csv(output_path + keys)
