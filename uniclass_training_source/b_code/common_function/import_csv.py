import pandas


def import_csv(
        list_files: list,
        source_path: str,
        input_files: dict
) \
        -> dict:
        
    for filename \
            in list_files:
        # print(filename)
        inputdf = \
            pandas.read_csv(source_path + filename)
        input_files.update({filename: inputdf})
        
    return \
        input_files
        
